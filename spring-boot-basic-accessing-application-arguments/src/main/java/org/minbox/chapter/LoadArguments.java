package org.minbox.chapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 加载启动项参数
 *
 * @author 恒宇少年
 */
@Component
public class LoadArguments {
    /**
     * 构造函数注入{@link ApplicationArguments}
     *
     * @param applicationArguments
     */
    @Autowired
    public LoadArguments(ApplicationArguments applicationArguments) {
        boolean isHaveSkip = applicationArguments.containsOption("skip");
        System.out.println("skip：" + isHaveSkip);
        System.out.println(applicationArguments.getOptionValues("skip"));
        List<String> arguments = applicationArguments.getNonOptionArgs();
        for (int i = 0; i < arguments.size(); i++) {
            System.out.println("非启动项参数：" + arguments.get(i));
        }
    }
}
