package org.minbox.chapter;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles({"beta", "test"})
class SpringBootBasicConfiguringRandomValuesApplicationTests {

    /**
     * logger instance
     */
    static Logger logger = LoggerFactory.getLogger(SpringBootBasicConfiguringRandomValuesApplication.class);

    @Autowired
    private RandomValuesConfig randomValuesConfig;
    @Value("${random.int}")
    private int number;

    @Value("${random.uuid}")
    private String uuid;

    @Value("${config.name}")
    private String name;

    @Test
    void testRandomConfig() {
        System.out.println(number);
        System.out.println(name);
        System.out.println(uuid);
        logger.info("int随机数：{}", randomValuesConfig.getNumber());
        logger.info("int设置最大值范围随机数：{}", randomValuesConfig.getMaxNumber());
        logger.info("long随机数：{}", randomValuesConfig.getLongValue());
        logger.info("long设置最大值范围随机数：{}", randomValuesConfig.getMaxLongValue());
        logger.info("随机uuid：{}", randomValuesConfig.getUuid());
    }

}
